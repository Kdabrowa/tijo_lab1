public class Main {

  public static void printFigure(Figure figure){
    figure.print();
  }
  public static void main(String[] args) {
    short age = 33;
    Girl monicaBellucci = new Girl();

    monicaBellucci.receiveFlower(new Rose());
    monicaBellucci.setAge(age);
    monicaBellucci.getFlower();
    monicaBellucci.age();

    Square square = new Square();
    System.out.println(" ");

    Triangle triangle = new Triangle();
    printFigure(square);
    printFigure(triangle);

  }
}
